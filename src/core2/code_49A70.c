#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802D10A4(Actor *this);

/* .data */
extern ActorAnimationInfo D_803673C0[];
extern ActorInfo D_80367404 = { 
    0x65, 0x56, 0x3EC,
    0x1, D_803673C0,
    func_802D10A4, func_80326224, func_80325888, 
    { 0x9, 0xC4}, 0x333, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0A00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0A38.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0AB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0B24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0B54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0CB4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0DDC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0F30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D0FC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_49A70/func_802D10A4.s")
