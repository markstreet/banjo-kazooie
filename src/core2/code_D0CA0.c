#include <ultra64.h>
#include "functions.h"
#include "variables.h"


Actor *func_80358344(ActorMarker *marker, Gfx **gfx, Mtx **mtx, Vtx **vtx);
void func_80358684(Actor *this);

/* .data */
extern ActorInfo D_803728A0 = { 
    0x253, ACTOR_380_SCARAB_BEETLE, ASSET_51B_MODEL_SCARAB_BEETLE, 
    0, NULL, 
    func_80358684, NULL, func_80358344, 
    { 0x0, 0x0}, 0, 1.0f, { 0x0, 0x0, 0x0, 0x0}
};


/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80357C30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80357CD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80357E34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80357F0C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_803582C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358304.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358344.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358490.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_803584BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358524.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358610.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D0CA0/func_80358684.s")
