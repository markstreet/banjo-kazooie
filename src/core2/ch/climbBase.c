#include <ultra64.h>
#include "functions.h"
#include "variables.h"


void func_802D77D4(Actor *this);

/* .data */
extern ActorInfo D_80367B20 = { 
    0x35, 0x26, 0x0, 
    0x1, NULL, 
    func_802D77D4, func_80326224, func_80325340, 
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/ch/climbBase/func_802D76E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/ch/climbBase/func_802D77D4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/ch/climbBase/func_802D7930.s")
